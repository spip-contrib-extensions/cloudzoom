<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://trac.rezo.net/spip/spip/ecrire/lang/
if (!defined("_ECRIRE_INC_VERSION")) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// A

'adjustx' => 'd&eacute;calage sur X',
'adjusty' => 'd&eacute;calage sur Y',

// B

'bottom' => 'au dessous',

// F

'false' => 'd&eacute;sactiv&eacute;',

// I

'inside' => '&agrave; l\'int&eacute;rieur',

// L

'left' => '&agrave; gauche',
'lensopacity' => 'opacit&eacute; de la lentille',

// O

'opacityexplain' => 'entre 0 et 1',

//P

'parametre' => 'Param&egrave;tres d\'affichage',
'position' => 'position',
'pixel' => 'en pixels',

//R

'right' => '&agrave; droite',

// S

'softfocus' => 'flou de la vignette',
'softfocusexplain' => 'd&eacute;sactive la teinte',
'showtitle' => 'afficher le titre',

//T

'titre_menu_cloudzoom' => 'CloudZoom',
'top' => 'au dessus',
'tint' => 'teinte',
'tintexplain' => 'au format hexadecimal (ex: #ffffff). d&eacute;sactiv&eacute; quand le flou de la vignette est actif',
'tintopacity' => 'opacit&eacute; de la teinte',
'true' => 'activ&eacute;',
'titleopacity' => 'opacit&eacute; du titre',

// Z
'zoom' => 'zoomer sur l\'image',
'zoomwidth' => 'largeur du zoom',
'zoomheight' => 'hauteur du zoom',
);

?>
